$(document).ready(function() {

	$('.slick').slick({
  		fade: true,
  		dots: false,
  		autoplay: true,
  		arrows: false
	 });

	$('a#fancybox').fancybox({
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false,
		'padding'		: 	0
	});
	 
	$('.offcanvas-toggle').on('click', function() {
	  $('body').toggleClass('offcanvas-expanded');
	});

	
	$(".fancybox").fancybox();
	document.querySelector( "#nav-toggle" )
	  .addEventListener( "click", function(){
	  this.classList.toggle( "active" );
	});

	$('#pop').on('shown.bs.modal', function () {
	  $('#pop').focus()
	})

	$(function() {
	  $('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});

	$('.map').click(function () {
    	$('.map iframe').css("pointer-events", "auto");
	});


});


